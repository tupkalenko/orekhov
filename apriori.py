import pandas as pd
import itertools as itert
import time
import copy

class SupportAndInvoices:
    def __init__(self):
        self.support = 0
        self.invoices = []

    def __repr__(self):
        return "" + str(self.support) + "/invoices" + str(self.invoices)

class Candidat:
    def __init__(self):
        self.items = list()
        self.supportAndInvoices = SupportAndInvoices

    def __init__(self, items, supportAndInvoices):
        self.items = items
        self.supportAndInvoices = supportAndInvoices

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.items == other.items
        else:
            return False

    def __copy__(self):
        return Candidat(self.items, self.supportAndInvoices)

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        strItems = str(self.items)
        return strItems + " support : " + str(self.supportAndInvoices.support) + " invoices : {}".format(self.supportAndInvoices.invoices)

class Solution:
    results = list()
    dictL = {}
    dictC = {}
    supports = {}
    minSupport = 0
    def calculate(self, excel, rows):
        self.initial = self.readExcel(excel, rows)
        self.minSupport = 3
        self.invoicesList = list(map(str, self.initial.InvoiceNo.unique()))
        # print("started with rows number : {}".format(rows))
        i = 1
        while True:
            currentL = self.generateL(i)
            if not currentL:
                break
            self.results.append(currentL)
            i += 1
        return self.results

    def readExcel(self, excel, rows):
        initial = pd.read_excel(excel, nrows=rows)
        initial.drop(list(initial.columns[3:]), axis=1, inplace=True)
        del initial['StockCode']
        return initial

    def countSupport(self, items):
        hashed = hash((tuple(sorted(items))))
        if hashed in self.supports:
            return self.supports[hashed]
        else:
            support = 0
            res = SupportAndInvoices()
            for i in range(len(self.invoicesList)):
                itemsByInvoice = self.initial.loc[self.initial['InvoiceNo'].astype(str) == self.invoicesList[i]]['Description'].values
                #if we take more than 8 elements - time complexity will be very big
                itemsByInvoice = itemsByInvoice[:8]
                itemsByInvoice = list(map(str, itemsByInvoice))
                if set(items).issubset(itemsByInvoice):
                    support += 1
                    res.support = support
                    res.invoices.append(self.invoicesList[i])
                    self.supports[hash(tuple(sorted(items)))] = res
            return res

    def getInvoices(self, initial):
        return list(map(str, initial.InvoiceNo.unique()))

    def generateC(self, i):
        if str(i) in self.dictC:
            return self.dictC[str(i)]
        candidates = list()
        if i == 1:
            c = self.initial.groupby("Description").count().reset_index()
            c.columns = ("Description", "count")
            for b in c['Description'].values.astype(str):
                items = list()
                items.append(b)
                candidates.append(Candidat(items, self.countSupport(items)))
            self.dictC[str(i)] = candidates
            return candidates
        candidatesLeft = self.generateL(i - 1)
        candidatesRight = copy.copy(candidatesLeft)
        curPos1 = 0
        while curPos1 <= len(candidatesLeft) - 1:
            candidatLeft = candidatesLeft[curPos1]
            curPos1 += 1
            curPos2 = curPos1
            while curPos2 <= len(candidatesRight) - 1:
                candidatRight = candidatesRight[curPos2]
                itemList = set()
                if i == 2:
                    itemList.update(candidatLeft.items[:])
                    itemList.update(candidatRight.items[:])
                if i >= 3:
                    itemsLeft = candidatLeft.items[:i - 2]
                    itemsRight = candidatRight.items[:i - 2]
                    kCondition = itemsLeft == itemsRight
                    if not kCondition:
                        break
                    itemList.update(candidatLeft.items[:])
                    itemList.update(candidatRight.items[i-2:])
                candidates.append(Candidat(sorted(list(itemList)), SupportAndInvoices()))
                curPos2 += 1
        self.dictC[str(i)] = candidates
        return candidates

    def generateL(self, i):
        if str(i) in self.dictL:
            return self.dictL[str(i)]
        l = list()
        if i < 1:
            return l
        c = self.generateC(i)
        if i == 1:
            for b in c:
                currSupport = self.countSupport(b.items).support
                if currSupport >= self.minSupport:
                    l.append(b)
            self.dictL[str(i)] = l
            return l
        for cand in range(len(c)):
            # all combinations and support check
            allSubsetsOk = True
            for index in range(len(c[cand].items), 0, -1):
                if not allSubsetsOk:
                    break
                for subset in itert.combinations(sorted(c[cand].items), index):
                    currSupport = self.countSupport(list(subset)).support
                    # check support of each subset
                    if currSupport < self.minSupport:
                        allSubsetsOk = False
                        break
            if allSubsetsOk:
                candidatSupport = self.countSupport(c[cand].items)
                # check support of the whole candidat item
                if candidatSupport.support >= self.minSupport:
                    candidat = Candidat(c[cand].items, candidatSupport)
                    l.append(candidat)
        self.dictL[str(i)] = l
        return l

    def generateRulesFor(self, c):
        rules = set()
        minConf = 0.6
        #generate all subsets n-1
        for index in range(len(c.items)-1, 0, -1):
            for a in itert.combinations(sorted(c.items), index):
                b = sorted(list(set(c.items) - set(a)))
                absup = self.countSupport((list(set(a).union(b)))).support
                asup = self.countSupport(list(a)).support
                confidence = float(absup)/float(asup)
                print ("confidence : {},  for a : {} , b : {}, a+b : {}, support for a : {} , support for a+b : {}"
                       .format(confidence,list(a),b,sorted(list(set(a).union(b))),
                               asup,absup))
                if confidence >= minConf:
                    rules.add(Rule(list(a), b, confidence))
        return rules

    def generateRules(self, ls):
        rules = list()
        for l in ls:
            for c in l:
                for rule in self.generateRulesFor(c):
                    rules.append(rule)
        return rules

class Rule:
    def __init__(self):
        self.a = set()
        self.b = set()
        self.conf = 0

    def __init__(self, a, b, conf):
        self.a = a
        self.b = b
        self.conf = conf

    def __repr__(self):
        return "{} => {} / confidence : {}".format(self.a, self.b, self.conf)


sol = Solution()
linesNumber = input("Enter number of items\n")
start = time.time()
res = sol.calculate("C:\\py\\Online retail.xls", int(linesNumber))
end = time.time()
for s in res:
    print("L : {}".format(s))
rules = sol.generateRules(res)
for r in rules:
    print("Rule : {}".format(r))
print("total execution time : {}".format(end - start))
