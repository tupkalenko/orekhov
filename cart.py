import pandas as pd
import time

class Solution:
    def calculate(self,csv, rows, initpos):
        self.initial = pd.read_csv(csv, nrows=rows)
        self.header = list(self.initial.columns.values[initpos:])
        data = self.initial.iloc[:,initpos:].values
        my_tree = self.build_tree(data)
        self.print_tree(my_tree)
        testing_data_size = rows/2
        testing_data = pd.read_csv(csv, skiprows = rows, nrows = testing_data_size).iloc[:,:].values
        for row in testing_data:
            print("Actual: %s. Predicted: %s" %
                  (row[-1], self.classify(row, my_tree)))

    def unique_vals(self, rows, col):
        return set([row[col] for row in rows])

    def class_counts(self, rows):
        counts = {}  # a dictionary of label -> count.
        for row in rows:
            # in our dataset format, the label is always the last column
            label = row[-1]
            if label not in counts:
                counts[label] = 0
            counts[label] += 1
        return counts

    class Question:
        def __init__(self, column, value, header):
            self.column = column
            self.value = value
            self.header = header

        def match(self, example):
            val = example[self.column]
            if self.is_numeric(val):
                return val >= self.value
            else:
                return val == self.value

        def is_numeric(self, value):
            return isinstance(value, int) or isinstance(value, float)

        def __repr__(self):
            condition = "=="
            if self.is_numeric(self.value):
                condition = ">="
            return "Is %s %s %s?" % (
                self.header[self.column], condition, str(self.value))

    class Leaf:
        def __init__(self, rows, solution):
            self.predictions = solution.class_counts(rows)

    class Decision_Node:
        def __init__(self,
                     question,
                     true_branch,
                     false_branch):
            self.question = question
            self.true_branch = true_branch
            self.false_branch = false_branch

    def partition(self, rows, question):
        true_rows, false_rows = [], []
        for row in rows:
            if question.match(row):
                true_rows.append(row)
            else:
                false_rows.append(row)
        return true_rows, false_rows


    def gini(self, rows):
        counts = self.class_counts(rows)
        impurity = 1
        for lbl in counts:
            prob_of_lbl = counts[lbl] / float(len(rows))
            impurity -= prob_of_lbl**2
        return impurity


    def info_gain(self, left, right, current_uncertainty):
        p = float(len(left)) / (len(left) + len(right))
        return current_uncertainty - p * self.gini(left) - (1 - p) * self.gini(right)

    def find_best_split(self, rows):
        best_gain = 0  # keep track of the best information gain
        best_question = None  # keep train of the feature / value that produced it
        current_uncertainty = self.gini(rows)
        n_features = len(self.header)  # number of columns

        for col in range(n_features-1):  # for each feature

            values = set([row[col] for row in rows])  # unique values in the column

            for val in values:  # for each value
                question = Solution.Question(col, val, self.header)
                true_rows, false_rows = self.partition(rows, question)
                if len(true_rows) == 0 or len(false_rows) == 0:
                    continue
                gain = self.info_gain(true_rows, false_rows, current_uncertainty)

                if gain >= best_gain:
                    best_gain, best_question = gain, question

        return best_gain, best_question

    def build_tree(self,rows):
        gain, question = self.find_best_split(rows)
        if gain == 0:
            return Solution.Leaf(rows,self)
        true_rows, false_rows = self.partition(rows, question)
        true_branch = self.build_tree(true_rows)
        false_branch = self.build_tree(false_rows)

        return Solution.Decision_Node(question, true_branch, false_branch)


    def print_tree(self, node, spacing=""):

        # Base case: we've reached a leaf
        if isinstance(node, Solution.Leaf):
            print (spacing + "Predict", self.print_leaf(node.predictions))
            return

        # Print the question at this node
        print (spacing + str(node.question))

        # Call this function recursively on the true branch
        print (spacing + '--> True:')
        self.print_tree(node.true_branch, spacing + "  ")

        # Call this function recursively on the false branch
        print (spacing + '--> False:')
        self.print_tree(node.false_branch, spacing + "  ")


    def classify(self, row, node):
        if isinstance(node, Solution.Leaf):
            return self.print_leaf(node.predictions)

        if node.question.match(row):
            return self.classify(row, node.true_branch)
        else:
            return self.classify(row, node.false_branch)


    def print_leaf(self, counts):
        total = sum(counts.values()) * 1.0
        probs = {}
        for lbl in counts.keys():
            probs[lbl] = str(int(counts[lbl] / total * 100)) + "%"
        return probs

sol = Solution()
linesNumber = input("Enter number of items\n")
initpos = input("Enter start feature\n")
start = time.time()
sol.calculate("C:\\py\\toy_dataset.csv", int(linesNumber), int(initpos))
end = time.time()
print("total execution time : {}".format(end - start))